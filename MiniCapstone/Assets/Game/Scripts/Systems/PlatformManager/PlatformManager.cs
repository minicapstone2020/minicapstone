﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum PlatformType
{
    Unidentified,
    SteeringUnit,
    ShootingUnit,
    CommandingUnit
}

public class PlatformManager : MonoBehaviour
{
    public PlatformType platformType { get; private set; } = PlatformType.Unidentified;

    private void Start()
    {
#if UNITY_IOS
        platformType = PlatformType.CommandingUnit;        
#elif UNITY_STANDALONE_WIN
        if (ArduinoRFID.ReadFromRFID.FoundRFID)
        {
            platformType = PlatformType.ShootingUnit;
        }
        else if (SteeringInput.SteeringInput.FoundSteeringUnit)
        {
            platformType = PlatformType.SteeringUnit;
        }
        else
        {
            platformType = PlatformType.Unidentified;
        }
#else
        platformType = PlatformType.Unidentified;        
#endif

        Debug.Log("Platform found " + platformType.ToString());
    }
}
