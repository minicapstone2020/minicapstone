﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
#if !UNITY_IOS
using System.IO.Ports;
#endif

namespace ArduinoRFID
{
    public class ReadFromRFID : MonoBehaviour
    {
        public static bool FoundRFID { get; private set; } = false;

#if !UNITY_IOS
        SerialPort ArduinoPort = new SerialPort("COM3", 9600); //Define Port

#endif
        int ReadValue = -1; //Gets 0 or 1 from the Arduino code.

        void Start()
        {
#if !UNITY_IOS
            ArduinoPort.Open();
            ArduinoPort.ReadTimeout = 100;
            if (ArduinoPort.IsOpen)
            {
                FoundRFID = true;
            }
#endif
            StartCoroutine(ReadFromPort());
        }

        IEnumerator ReadFromPort()
        {
            while (true)
            {
#if !UNITY_IOS
                if (ArduinoPort.IsOpen)
                {
                    try
                    {
                        //Debug.Log(ArduinoPort.ReadByte());
                        ReadValue = ArduinoPort.ReadByte(); //0 or 1 depending on the value written by the Arduino program
                    }
                    catch (System.Exception ex)
                    {
                        //Debug.Log(ex.Message);
                        ReadValue = -1;
                    }
                }
#endif
                yield return new WaitForSeconds(0.0f);
            }
        }

        public int ReturnReadValue()
        {
            return ReadValue;
        }
    }
}