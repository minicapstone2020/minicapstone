﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(ArduinoRFID.ReadFromRFID))]
public class AuthorizationCheck : MonoBehaviour
{
    [SerializeField]
    Image PositiveIndicator;
    [SerializeField]
    Color PositiveColor;
    [SerializeField]
    Image NegativeIndicator;
    [SerializeField]
    Color NegativeColor;

    ArduinoRFID.ReadFromRFID RFIDReader;

    // Start is called before the first frame update
    void Start()
    {
        RFIDReader = GetComponent<ArduinoRFID.ReadFromRFID>();
    }

    // Update is called once per frame
    void Update()
    {
        int value = RFIDReader.ReturnReadValue();
        if (value >= 0)
        {
            if (value == 0)
            {
                PositiveIndicator.color = Color.white;
                NegativeIndicator.color = NegativeColor;
            }
            if (value == 1)
            {
                PositiveIndicator.color = PositiveColor;
                NegativeIndicator.color = Color.white;
            }
        }
        else
        {
                PositiveIndicator.color = Color.white;
                NegativeIndicator.color = Color.white;
        }
    }
}
