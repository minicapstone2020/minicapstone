﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SteeringInput
{
    public enum DPadState { CENTER, UP, UPRIGHT, RIGHT, DOWNRIGHT, DOWN, DOWNLEFT, LEFT, UPLEFT, NONE };

    public class SteeringInput : MonoBehaviour
    {
        LogitechGSDK.LogiControllerPropertiesData properties;
        LogitechGSDK.DIJOYSTATE2ENGINES rec;
        bool isHardwareOperational = false;

        public static bool FoundSteeringUnit { get; private set; } = false;
        // Start is called before the first frame update
        void Start()
        {
#if !UNITY_IOS
           FoundSteeringUnit = LogitechGSDK.LogiSteeringInitialize(false);
            if(FoundSteeringUnit)
            {
                Debug.Log("SteeringInit:" + FoundSteeringUnit);
            }
#endif
        }

        // Update is called once per frame
        void Update()
        {
#if !UNITY_IOS
            if (LogitechGSDK.LogiUpdate() && LogitechGSDK.LogiIsConnected(0))
            {
                rec = LogitechGSDK.LogiGetStateUnity(0);
                isHardwareOperational = true;
            }
#endif
        }

        //Returns Steering axis value between (left rotation) -450 degrees and (right rotation) 450 degrees (lock to lock) from the starting position of 0 degrees.
        public float GetSteeringAxis()
        {
            if (isHardwareOperational)
            {
                return (ExtensionMethods.Remap(rec.lX, -32767, 32768, -5.0f, 5.0f));    //5 axes of 90 degrees on each side, adding up to a total of 450 degrees. Kept so in order to take input only for the first 90 degrees i.e. -1.0 to 1.0
                //return rec.lX;
            }
            else
            {
                return 0;
            }
        }

        //Returns the Clutch press from 0 to 100; 0 being the release state and 100 being the fully pressed state.
        public float GetClutchAxis()
        {
            if (isHardwareOperational)
            {
                return Mathf.Abs(ExtensionMethods.Remap(rec.rglSlider[0], -32767, 32768, -1.0f, 0));
                //return rec.rglSlider[0];
            }
            else
            {
                return 0;
            }
        }

        //Returns the Brake press from 0 to 100; 0 being the release state and 100 being the fully pressed state.
        public float GetBrakeAxis()
        {
            if (isHardwareOperational)
            {
                return Mathf.Abs(ExtensionMethods.Remap(rec.lRz, -32767, 32768, -1.0f, 0));
                //return rec.lRz;
            }
            else
            {
                return 0;
            }
        }

        //Returns the Accelerator press from 0 to 100; 0 being the release state and 100 being the fully pressed state.
        public float GetAcceleratorAxis()
        {
            if (isHardwareOperational)
            {
                return Mathf.Abs(ExtensionMethods.Remap(rec.lY, -32767, 32768, -1.0f, 0));
                //return rec.lY;
            }
            else
            {
                return 0;
            }
        }

        public DPadState GetDPadState()
        {
            if (isHardwareOperational)
            {
                switch (rec.rgdwPOV[0])
                {
                  case (0): return DPadState.UP;
                  case (4500): return DPadState.UPRIGHT;
                  case (9000): return DPadState.RIGHT;
                  case (13500): return DPadState.DOWNRIGHT;
                  case (18000): return DPadState.DOWN;
                  case (22500): return DPadState.DOWNLEFT;
                  case (27000): return DPadState.LEFT;
                  case (31500): return DPadState.UPLEFT;
                  default: return DPadState.CENTER;   //is the default state, even if not pressed.
                }
            }
            else
            {
                return DPadState.NONE;
            }
        }

        public int GetButtonPressed()
        {
            if (isHardwareOperational)
            {
                for (int i = 0; i < 128; i++)
                {
                    if (rec.rgbButtons[i] == 128)
                    {
                        //buttonStatus += "Button " + i + " pressed\n";
                        return i;
                    }

                }
            }

            return -1;
        }

    }
}