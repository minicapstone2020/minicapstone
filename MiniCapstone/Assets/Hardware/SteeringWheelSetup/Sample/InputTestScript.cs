﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(SteeringInput.SteeringInput))]
public class InputTestScript : MonoBehaviour
{
    SteeringInput.SteeringInput steeringInput;

    public Slider steeringSlider;
    public Slider clutchSlider;
    public Slider brakeSlider;
    public Slider acceleratorSlider;

    public Image DPad;
    public Image[] Buttons;

    private void Start()
    {
        steeringInput = GetComponent<SteeringInput.SteeringInput>();
    }

    void Update()
    {
        steeringSlider.value = steeringInput.GetSteeringAxis();
        clutchSlider.value = steeringInput.GetClutchAxis();
        brakeSlider.value = steeringInput.GetBrakeAxis();
        acceleratorSlider.value = steeringInput.GetAcceleratorAxis();

        if (steeringInput.GetDPadState() != SteeringInput.DPadState.NONE && steeringInput.GetDPadState() !=  SteeringInput.DPadState.CENTER)
        {
            DPad.gameObject.SetActive(true);
            Debug.Log(steeringInput.GetDPadState().ToString());
        }
        else
        {
            DPad.gameObject.SetActive(false);
        }

        for (int i = 0; i < Buttons.Length; i++)
        {
            if (steeringInput.GetButtonPressed() == i)
            {
                Buttons[i].gameObject.SetActive(true);
            }
            else
            {
                Buttons[i].gameObject.SetActive(false);
            }
        }
    }
}
